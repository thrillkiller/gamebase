package com.szk.example.GameBase.Controlers;

import com.szk.example.GameBase.Models.Game;
import com.szk.example.GameBase.Models.Genre;
import com.szk.example.GameBase.Repositories.GameRepo;
import com.szk.example.GameBase.Repositories.GenreRepo;
import com.szk.example.GameBase.dataValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;


@SuppressWarnings("unused")
@Controller
public class GameController {
    @Autowired
    private GameRepo gameRepository;
    @Autowired
    private GenreRepo genreRepository;

    public void setGameRepository(GameRepo gameRepository) {
        this.gameRepository = gameRepository;
    }

    public void setGenreRepository(GenreRepo genreRepository) {
        this.genreRepository = genreRepository;
    }

    //Games part
    @GetMapping("/games")
    @ResponseBody
    public ModelAndView gamesPage() {
        ModelAndView modelAndView = new ModelAndView("games");
        ArrayList<String> gamesList = new ArrayList<>();
        ArrayList<String> genresList = new ArrayList<>();

        genresList.clear();
        for (Genre g : genreRepository.findAll()) {
            genresList.add(g.getName());
        }

        for (Game g : gameRepository.findAll()) {
            gamesList.add(g.toString());
        }
        modelAndView.addObject("genreSize", genreRepository.count());
        modelAndView.addObject("size", gameRepository.count());
        modelAndView.addObject("genresList", genresList);
        modelAndView.addObject("games", gamesList);
        return modelAndView;
    }

    @PostMapping
    @ResponseBody
    public ModelAndView onAddGame(@RequestParam Map<String, String> parameters) {
        String operationString = "";
        ModelAndView modelAndView = new ModelAndView("games");
        ArrayList<String> gamesList = new ArrayList<>();
        ArrayList<String> genresList = new ArrayList<>();
        try {
            gameRepository.save(parseParametersIntoGame(parameters));
        } catch (dataValidationException e) {
            operationString += "Failed to add " + parameters.get("gameName") + " to database";
            operationString += "\n" + e.getMessage();
        }
        for (Game g : gameRepository.findAll()) {
            gamesList.add(g.toString());
        }
        for (Genre g : genreRepository.findAll()) {
            genresList.add(g.getName());
        }
        modelAndView.addObject("genreSize", genreRepository.count());
        modelAndView.addObject("size", gameRepository.count());
        modelAndView.addObject("genresList", genresList);
        modelAndView.addObject("games", gamesList);
        modelAndView.addObject("operationString", operationString);
        return modelAndView;
    }

    private Game parseParametersIntoGame(Map<String, String> parameters) throws dataValidationException {
        Game game = new Game();
        //Check that what can't be wrong
        String name = parameters.get("gameName");
        if (gameRepository.doesExist(name))
            throw new dataValidationException("It already exists in base");
        HashSet<Genre> genresSet = new HashSet<>();
        for (Genre g : genreRepository.findAll()) {
            if (parameters.containsKey(g.getName()))
                genresSet.add(g);
        }
        if (genresSet.isEmpty())
            throw new dataValidationException("No genres specified");
        game.setGenresSet(genresSet);

        double grade;
        int year;
        try {
            grade = Double.parseDouble(parameters.get("gameGrade"));
            year = Integer.parseInt(parameters.get("gameYear"));
        } catch (NumberFormatException e) {
            throw new dataValidationException("Type numbers in specified areas");
        }
        if (!game.setGrade(grade))
            throw new dataValidationException("Grade value is not between 0 and 10");
        if (!game.setYear(year))
            throw new dataValidationException("Year is not valid");

        if (parameters.containsKey("gamePossesed"))
            game.setPosesed(true);
        else
            game.setPosesed(false);

        game.setName(name);

        return game;
    }

    @PostMapping("/games/delete/{id}")
    @ResponseBody
    public String onDeleteGame(@PathVariable("id") int id) {
        gameRepository.delete(id);
        return "Succesfully deleted";
    }

}
