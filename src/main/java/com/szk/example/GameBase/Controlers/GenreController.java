package com.szk.example.GameBase.Controlers;

import com.szk.example.GameBase.Models.Genre;
import com.szk.example.GameBase.Repositories.GenreRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@SuppressWarnings("unused")
@Controller
public class GenreController {

    @Autowired
    private GenreRepo genreRepository;

    public void setGenreRepository(GenreRepo genreRepository) {
        this.genreRepository = genreRepository;
    }

    //Genres part
    @GetMapping(value = "/genres")
    @ResponseBody
    public ModelAndView genresPage() {
        ModelAndView modelAndView = new ModelAndView("genres");
        ArrayList<String> genresNames = new ArrayList<>();
        /*Used only to be passed to view.
         *Normally i would pass as object to view repository but I don't know to access property of java class in Twig */
        long size = genreRepository.count();
        if (size != 0) {
            genresNames.clear();
            for (Genre g : genreRepository.findAll()) {
                genresNames.add(g.getName());
            }
        }
        modelAndView.addObject("size", size);
        modelAndView.addObject("genresList", genresNames);
        return modelAndView;
    }

    @PostMapping("/genres")
    @ResponseBody
    public ModelAndView onGenreAdd(@RequestParam("genreName") String gName) {
        String operationStatus;
        ModelAndView modelAndView = new ModelAndView("genres");
        ArrayList<String> genresNames = new ArrayList<>();
        boolean exists = false;
        long size = genreRepository.count();

       /* for (Genre g : genreRepository.findAll()) {
            if (g.getName().equals(gName))
                exists = true;
        }*/
        if (!genreRepository.doesExist(gName)) {
            Genre genre = new Genre();
            genre.setName(gName);

            if (genreRepository.save(genre) == null) {//Failed to save
                operationStatus = "Failed to add genre " + gName;
            } else//Everything was ok
            {
                operationStatus = gName + " was added succesfully to database";
            }
            System.out.println(gName);
        } else {
            operationStatus = gName + " already exists in base!";
        }
        genresNames.clear();

        for (Genre g : genreRepository.findAll()) {
            genresNames.add(g.getName());
        }
        modelAndView.addObject("operationStatus", operationStatus);
        modelAndView.addObject("size", size);
        modelAndView.addObject("genresList", genresNames);
        return modelAndView;
    }

    @PostMapping("/genres/delete/{name}")
    @ResponseBody
    public String onDeleteGame(@PathVariable("name") String name) {
        if (genreRepository.hasGamesAttached(name)) {
            return name + " has associated games and cannot be deleted";
        } else {
            for (Genre g : genreRepository.findAll()) {
                if (g.getName().equals(name))
                    genreRepository.delete(g);
            }
            return name + " was deleted";
        }
    }

}
