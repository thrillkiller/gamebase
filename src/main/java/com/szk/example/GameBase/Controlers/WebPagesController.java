package com.szk.example.GameBase.Controlers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@SuppressWarnings("unused")
@Controller
public class WebPagesController {
    //Controller for pages not affiliated with models etc.
    //For main page
    @GetMapping("/")
    @ResponseBody
    public ModelAndView indexPage() {
        return new ModelAndView("index");
    }

}
