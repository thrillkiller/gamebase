package com.szk.example.GameBase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan
@SpringBootApplication
@EnableJpaRepositories
public class GameBaseApplication {


	public static void main(String[] args) {
		SpringApplication.run(GameBaseApplication.class, args);
	}
}
