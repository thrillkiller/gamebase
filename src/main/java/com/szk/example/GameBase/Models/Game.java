package com.szk.example.GameBase.Models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Game {
    @GeneratedValue
    @Id
    private int gameId;

    @Column(unique = true)
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    //@JoinColumn(name = "genreId" )
    private Set<Genre> genresSet;

    @Column
    private boolean posesed;

    @Column
    private double grade;

    @Column
    private int year;

    public Game(String name, Set<Genre> genres, double grade, int year) {
        this.name = name;
        this.genresSet.clear();
        this.genresSet.addAll(genres);
        this.grade = grade;
        this.year = year;
        genresSet = new HashSet<>();
    }

    public Game() {
        genresSet = new HashSet<>();
    }
    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGenresSet(Set<Genre> genresSet) {
        this.genresSet.clear();
        this.genresSet.addAll(genresSet);
    }

    public String getGenresString() {
        String value = "";
        if (genresSet.isEmpty()) {
            value = "None";
        } else {
            for (Genre g : genresSet) {
                value += g.getName();
                if (genresSet.iterator().hasNext())
                    value += " , ";
            }
        }
        return value;
    }

    public double getGrade() {
        return grade;
    }

    public boolean setGrade(double grade) {
        boolean ret = false;
        if (grade >= 0.0 && grade <= 10.0) {
            this.grade = grade;
            ret = true;
        }
        return ret;
    }

    public void setPosesed(boolean posesed) {
        this.posesed = posesed;
    }

    public boolean isPosesed() {
        return posesed;
    }

    public int getYear() {
        return year;
    }

    public boolean setYear(int year) {
        if (year < 1970 || year > LocalDate.now().getYear())
            return false;
        else {
            this.year = year;
            return true;
        }
    }

    public String toString() {
        String s = "Id = " + String.valueOf(gameId) + " ";
        s += name + " ";
        s += String.valueOf(year) + " ";
        s += getGenresString() + " ";
        s += String.valueOf(grade) + " ";
        if (posesed)
            s += "Owned";
        else
            s += "Not owned";
        return s;
    }

}
