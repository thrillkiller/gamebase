package com.szk.example.GameBase.Models;

import javax.persistence.*;

@Entity
public class Genre {
    @GeneratedValue
    @Id
    private int genreId;

    @Column(unique = true)
    private String name;

    public Genre() {

    }

    public Genre(String name) {
        this.name = name;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
