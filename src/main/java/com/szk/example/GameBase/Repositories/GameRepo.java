package com.szk.example.GameBase.Repositories;

import com.szk.example.GameBase.Models.Game;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface GameRepo extends CrudRepository<Game, Integer> {

    @Query("select case when count(name) > 0 then true else false end from Game where name = :givenName ")
    boolean doesExist(@Param("givenName") String name);
}