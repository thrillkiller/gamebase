package com.szk.example.GameBase.Repositories;

import com.szk.example.GameBase.Models.Genre;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface GenreRepo extends CrudRepository<Genre, Integer> {

    @Query("select case when count(name) > 0 then true else false end from Genre where name = :givenName ")
    boolean doesExist(@Param("givenName") String name);

    @Query("select case when count(ga.name) > 0 then true else false end from Game ga inner join ga.genresSet as ge where ge.name = :givenName")
    boolean hasGamesAttached(@Param("givenName") String name);
}